#include <stdio.h>

void test1(void)
{
    printf("in test1.\n");
    sleep(1);
}

void test2(void)
{
    test1();
    printf("in test2.\n");
    sleep(2);
}

void test3(void)
{
    test1();
    test2();
    printf("in test3.\n");
    sleep(3);
}

int main(int argc, char *argv[])
{
    printf("hello world!!!\n");
    test1();
    test2();
    test3();
    return 0;
}
